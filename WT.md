# Werkstofftechnik
                            
<center>

![Bild-Titel](https://www.fachhochschule.de/Pic/Content/technik_werkstoff_DE_BOTTOM/Materialwissenschaften%20und%20Werkstofftechnik%20Fotolia_27457771_S.jpg?1560251579)                    
                        
</center>
                        
---
                        
## 1 Werkstoffstruktur: Die innere Ordnung der Werkstoffe

### 1.1 Lernziele zu Kapitel 1

• Sie erklären die wichtigsten Kristallstrukturen der Metalle und die daraus resultierenden spezifischen Eigenschaften.

• Sie erläutern die verschiedenen Arten von Gitterfehlern und ihre Bedeutung für Metalle.

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.

• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--


### 1.2 Der Aufbau von Werkstoffen im perfekten Gitter

Der strukturelle Aufbau der Werkstoffe bestimmt die Werkstoffeigenschaften und damit
die Anwendungsgebiete. Das betrifft alle strukturellen Aspekte vom „Kleinsten“ (Bindungen,
Atomanordnung, Gefüge, …) bis zum „Größten“ (Bauteile: Geometrie, Oberflächenqualität, …).
Nach der Entdeckung der Röntgenstrahlung (X-Strahlen, X-Rays) durch Wilhelm Conrad
Röntgen im Jahr 1895 wurden etwa ab dem Beginn der 20. Jahrhunderts Kristalle mit
Röntgenstrahlen untersucht. Ziel war die Gewinnung von Erkenntnissen zu Gitteraufbau und
Kristallstruktur (Röntgenfeinstrukturanalyse).

<center>
                        
![Bild-Titel](‪C:\Users\NK\Desktop\WT\1.jpg) 
                        
</center>

--

Amorphe Strukturen zeigen z. B. Kunststoffe und Glas. Es gibt nur Nahordnung (keine
Fernordnung). Die Atome sind regellos angeordnet. Es besteht kein Gitter!
Zur Vereinfachung betrachten wir zunächst ideale Kristalle mit einer mathematisch
beschreibbaren perfekten Gitterstruktur (siehe Abb. 1.2.1 Mitte und rechts):

• Kristalline Strukturen gibt es z. B. bei Metallen und Diamant. Hier liegt Nahordnung und
Fernordnung vor. Die Atome sind 3-dimensional periodisch angeordnet
-> Kristallgitter!

• Die Elementarzelle ist die kleinste Baueinheit eines Kristallgitters. Sie enthält alle Informationen zum Aufbau des Raumgitters (Gitterparameter a, b, c und Achsenwinkel α, β, γ).

• Das Raumgitter entsteht durch 3-dimensional periodisches Aneinanderreihen von
Elementarzellen.

• Die Kristallgittertypen der wirtschaftlich wichtigsten Metalle sind kfz, krz und hdp.
In diesem Kapitel erfahren wir schon ansatzweise etwas über die plastische (bleibende)
Verformung von Metallen. Später werden wir diesen wichtigen Punkt weiter vertiefen.

--

Metalle haben im „Normalfall“ im festen Zustand nur eine Gitterstruktur, die sich nicht ändert.
So hat Magnesium im festen Zustand immer eine hdp-Struktur, unabhängig von der Temperatur.
Manche Metalle verhalten sich jedoch polymorph. Diese Metalle weisen bei verschiedenen
Temperaturen verschiedene Gitterstrukturen auf. Dies spielt in der Praxis eine wichtige Rolle
bei Metallen wie Eisen, z. B. die Umwandlung krz ↔ kfz. Bei dieser Änderung der Gitterstruktur
ändert sich auch die Packungsdichte (Volumenänderung!). Das ist entscheidend für viele
Prozesse wie z. B. die Wärmebehandlung von Stahl. Stahl ist eine Legierung aus Eisen (Fe)
und Kohlenstoff (C). Auch Ti, Co und Zr sind polymorph.

Wichtige Kennzeichen der Kristallgittertypen sind z. B. die Zahl ihrer Gleitsysteme (ein GS =
Kombination aus Gleitebene und zugehörige Gleitrichtung) und ihre Packungsdichte (PD).
Eine hohe Packungsdichte bedeutet geringe Kräfte zur plastischen Verformung (und
umgekehrt). Deshalb sind besonders dicht mit Atomen besetzte Ebenen Gleitebenen
(bevorzugte Ebenen der plastischen Verformung, energetisch günstig zum Gleiten).

Gleitebenen bewegen sich energetisch günstig in bevorzugten Gleitrichtungen durch „Täler“
in den Gleitebenen (= „Täler“ zwischen benachbarten Atomreihen) Das sind Richtungen mit
größter Packungsdichte der Atome.
Viele Gleitsysteme bedeuten viele Gleitmöglichkeiten und damit eine gute plastische
Verformbarkeit (und umgekehrt). Genaueres zur plastischen Verformung folgt später.



--

### 1.3 Richtungsabhängigkeit von Werkstoffeigenschaften

Elementarzellen (und deshalb auch Gitter) zeigen in Abhängigkeit von der betrachteten
Richtung verschiedene Abstände zwischen den Atomen und somit richtungsabhängige
Eigenschaften. Je geringer die Atomabstände in einer Richtung sind, desto größer ist z. B. der
E-Modul für diese Richtung und umgekehrt. Auch andere mechanische Eigenschaften wie die
Dehn- / Streckgrenze, die Zugfestigkeit und die plastische Verformbarkeit (Bruchdehnung)
hängen von der betrachteten Richtung im Kristall ab.

--

Erläuterungen zur Richtungsabhängigkeit von Werkstoffeigenschaften:

Quasiisotroper Polykristall (Vielkristall):

o Jeder Kristall hat für sich betrachtet eine Vorzugsrichtung (Pfeil).

o Die Vorzugsrichtungen sind jedoch statistisch verteilt.

o Nach außen hin gibt es deshalb keine Vorzugrichtung (Kristall nach außen isotrop).

Anisotroper Polykristall (Vielkristall mit Textur):

o Jeder Kristall hat für sich betrachtet eine Vorzugsrichtung (Pfeil).

o Die Vorzugsrichtungen sind alle in eine ähnliche Richtung orientiert.

o Nach außen hin gibt es deshalb eine Vorzugrichtung (Kristall nach außen anisotrop).

Anisotroper Einkristall:

o Der Kristall hat keine Korngrenzen und insgesamt nur eine Vorzugsrichtung (Pfeil).

o Nach außen hin gibt es deshalb eine Vorzugrichtung (Kristall nach außen anisotrop).

--

Texturen entstehen durch plastische Verformung (z. B. Kaltwalzen von Blechen, Ziehen von
Drähten) oder die gerichtete Erstarrung von Schmelzen.
Erwünscht sind Texturen bei kaltgezogenen Drähten, Klaviersaiten, Nägeln (hohe Festigkeit).
Unerwünscht sind Texturen in der Umformtechnik (Tiefziehen von Blechen, …).

---

## 2 Mechanische Eigenschaften von Metallen

### 2.1 Lernziele zu Kapitel 2

• Sie beschreiben das elastische und plastische Verhalten von Metallen und verwenden
dazu die passenden Werkstoffkennwerte.

• Sie erklären, wie sich die unterschiedlichen Gitterfehler auf makroskopischer und
mikroskopischer Ebene auf das Werkstoffverhalten auswirken.

• Sie erläutern wichtige Mechanismen zur Festigkeitssteigerung von Metallen.

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.

• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--

### 2.2 Elastische Verformung (linear elastisch, energieelastisch)
Eine elastische Verformung liegt vor, wenn nach Be- und Entlastung eines Bauteils seine
Atome nur wenig (nicht bleibend) aus ihrer Position ausgelenkt wurden (siehe Abbildung):
1. Startpunkt.
2. Elastische Verformung (Hooke´sche Gerade, unterhalb der Dehn- / Streckgrenze).
3. Endpunkt = Startpunkt 1. Die elastische Dehnung εelastisch geht komplett zurück.

In der Praxis ist die elastische Verformung wichtig für Bauteile wie Zahnräder, Wellen, Federn.

-- 
### 2.3 Plastische Verformung
Eine plastische Verformung liegt vor, wenn nach Be- und Entlastung eines Bauteils seine
Atome bleibend aus ihrer Position ausgelenkt wurden. Nach Entlastung bleibt somit eine
plastische Verformung zurück (siehe Abbildung):
1. Startpunkt.
2. Elastische Verformung (Hooke´sche Gerade, unter der Dehn- / Streckgrenze).
Hinweis: Maximale elastische Verformung für Metalle: nur ca. 0,1 bis 0,2 Prozent!
3. Elastische und plastische Verformung (über der Dehn- / Streckgrenze).
4. Endpunkt. Die elastische Dehnung εelastisch ging zurück und die plastische Dehnung
εplastisch bleibt bestehen.

In der Praxis ist die plastische Verformung wichtig für die Umformtechnik (z. B. Walzen,
Tiefziehen, Gewinderollen) und als Verformungsreserve z. B. bei Wellen und Zahnrädern.

-- 
#### 2.3.1 Die „übliche“ Bewegung von Versetzungen

Eine plastische Verformung (makroskopisch mit dem Auge sichtbar) bedeutet das Wandern
sehr vieler Versetzungen innerhalb der Metalls (also auf der mikroskopischen Ebene im
Werkstoff). Plastische Verformung ist auch über Zwillingsbildung möglich.
Die Versetzungsbewegung benötigt ausreichend hohe Schubspannungen (τ > τkritisch) auf
der Kristallgitterebene. Die Schubspannung τkritisch kann somit als eine Art „Streckgrenze
für die Versetzungsbewegung auf Kristallgitterebene“ aufgefasst werden.
Eine technische Legierung von etwa einem Kubikzentimeter Größe enthält viele tausende bis
hunderttausende Kilometer an Versetzungslinien. Nach einer plastischen Verformung kann die
Versetzungsdichte auch ca. um den Faktor 10 bis 100 oder noch höher sein.
Die Zahl der Versetzungen nimmt also mit zunehmender plastischer Verformung (> Re, Rp 0,2)
immer mehr zu. Die Versetzungen behindern sich beim Wandern zunehmend gegenseitig. Die
weitere plastische Verformung wird immer schwieriger, erfordert also immer höhere Kräfte.
Es kommt zur (Kalt-)Verfestigung durch (Kalt-)Verformung (= Verformungsverfestigung).
Versetzungen haben somit einen „zweischneidigen“ Charakter: Sie ermöglichen die
plastische Verformung. Zu viele Versetzungen erschweren diese jedoch!

--- 

## 3 Thermisch aktivierte Vorgänge
### 3.1 Lernziele zu Kapitel 3

• Sie erklären die <green> Diffusion , wenden einfache </green>Gleichungen zur Diffusion korrekt an und
erläutern Praxisbeispiele.

• Sie beschreiben die Erholung und die Rekristallisation als wichtige Arten der
Wärmebehandlung bei Metallen zur Beseitigung von Gitterfehlern.

• Sie stellen die Verformung von Metallen bei erhöhter Temperatur dar (Kriechen).

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.


• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--

## 3.2 Diffusion
Diffusion bei Metallen ist ein thermisch aktivierter Platzwechsel von Atomen und / oder
Leerstellen im Gitter.
Atome in Metallen <green>schwingen</green> um ihre Ruhelage und stoßen dabei immer wieder mit
benachbarten Atomen zusammen. Dabei kommt es zum Energieaustausch zwischen den
Atomen. Es gibt somit Atome mit hoher und niedriger Energie. Atome mit hoher Energie können
(falls sie zufällig in der richtigen Richtung unterwegs sind) ihren Platz im Gitter verlassen und
diesen mit einer Leerstelle tauschen. Dann hat sich das Atom um einen Platz in die eine
Richtung bewegt und die Leerstelle in die entgegengesetzte Richtung. „Hohe Energie“ des
Atoms bedeutet, dass die Aktivierungsenergie zum Verlassen des Gitterplatzes vorhanden ist.
Die Aktivierungsenergie für die Diffusion kommt also aus der Schwingung der Atome und
solche Prozesse heißen deshalb „thermisch aktiviert“. Weitere Beispiele für thermisch
aktivierte Prozesse sind z. B. die Erholung und Rekristallisation von Metallen.

Damit folgt: Da bei hoher Temperatur viel mehr Schwingungsenergie zur Verfügung steht,
funktioniert Diffusion bei hoher Temperatur viel leichter und schneller als bei niedriger
Temperatur. Zusätzlich gibt es bei hoher Temperatur viel mehr Leerstellen, die für den
Platzwechsel von Atomen genutzt werden können.
Warum findet Diffusion im Werkstoff statt?
Werkstoffe sind bestrebt, einen energetisch günstigen Zustand einzunehmen
(thermodynamischer Gleichgewichtszustand). Steht genügend Energie zur Verfügung und auch
ausreichend Zeit, wird sich der Werkstoff in Richtung des thermodynamischen Gleichgewichts
bewegen. Energetisch günstig ist es in der Regel, wenn Fremdatome möglichst gleichmäßig im Werkstoff verteilt sind (Ausnahme: Entmischungsprozesse, werden nicht vertieft).

--

**Es gibt zwei grundlegende Diffusionsmechanismen:**




<left>
Wanderung über Zwischengitterplätze:
•Einlagerungsatome („kleine Fremdatome“) wandern über Zwischengitterplätze. Diese
Atome sind zwar oft klein (Beispiel C), passen aber oft nicht genau in die Lücken des
Wirtsgitters (Beispiel Fe) und erzeugen somit lokal Spannungen. Auch beim Wandern
müssen sich die Zwischengitteratome zwischen anderen Atomen „durchzwängen“.
</left>


<right>
Wanderung über Leerstellen: <br>
•Bei reinen Metallen wandern die Atome des eigenen Gitters und tauschen ihren
Gitterplatz mit Leerstellen (z. B. Diffusion von Fe in reinem Fe).

•Bei Legierungen wandern die Fremdatome im Substitutionsmischkristall und tauschen
ihren Gitterplatz mit Leerstellen (z. B. Fremddiffusion von Ni in Fe).
</right>

--

## Video zum Skript

<center>

<iframe allowfullscreen src="https://mstream.hm.edu/paella/ui/watch.html?id=cab2381e-cf05-4461-b012-625c820172fd" style="border:0px #FFFFFF none;" name="Paella Player" scrolling="no" frameborder="0" marginheight="0px" marginwidth="0px" width="460" height="259"></iframe>

</center>


---

<script data-quiz>
				quiz = {"info": {
								"name":    "Teste dein Wissen!",
								"main":    "Versuchen Sie mindestens 80% der Fragen richtig zu beantworten!",
								"level1":  "Jeopardy Ready",
								"level2":  "Jeopardy Contender",
								"level3":  "Jeopardy Amateur",
								"level4":  "Jeopardy Newb",
								"level5":  "Stay in school, kid..." // no comma here
						},
						"questions": [
								{ // Question 1 - Multiple Choice, Single True Answer
										"q": "Warum steigern punktförmige Gitterfehler bei Metallen deren Festigkeit und Härte?",
										"a": [
												{"option": "ASDSADASD",      "correct": false},
												{"option": "DAWDWADWA",     "correct": true},
												{"option": "ADWSADSS",      "correct": false},
												{"option": "ASDSEEDFAS",     "correct": false} // no comma here
										],
										"correct": "<p><span>Richtig!</span> Deswegen!</p>",
										"incorrect": "<p><span>Falsch.</span> Deswegen</p>" // no comma here
								},
								{ // Question 2 - Multiple Choice, Multiple True Answers, Select Any
	"q": "Which of the following best represents your preferred breakfast?",
	"a": [
		{"option": "Bacon and eggs",               "correct": false},
		{"option": "Fruit, oatmeal, and yogurt",   "correct": true},
		{"option": "Leftover pizza",               "correct": false},
		{"option": "Eggs, fruit, toast, and milk", "correct": true} // no comma here
	],
	"select_any": true,
	"correct": "<p><span>Nice!</span> Your cholestoral level is probably doing alright.</p>",
	"incorrect": "<p><span>Hmmm.</span> You might want to reconsider your options.</p>" // no comma here
},
{ // Question 3 - Multiple Choice, Multiple True Answers, Select All
	"q": "Where are you right now? Select ALL that apply.",
	"a": [
		{"option": "Planet Earth",           "correct": true},
		{"option": "Pluto",                  "correct": false},
		{"option": "At a computing device",  "correct": true},
		{"option": "The Milky Way",          "correct": true} // no comma here
	],
	"correct": "<p><span>Brilliant!</span> You're seriously a genius, (wo)man.</p>",
	"incorrect": "<p><span>Not Quite.</span> You're actually on Planet Earth, in The Milky Way, At a computer. But nice try.</p>" // no comma here
},
{ // Question 4
	"q": "How many inches of rain does Michigan get on average per year?",
	"a": [
		{"option": "149",    "correct": false},
		{"option": "32",     "correct": true},
		{"option": "3",      "correct": false},
		{"option": "1291",   "correct": false} // no comma here
	],
	"correct": "<p><span>Holy bananas!</span> I didn't actually expect you to know that! Correct!</p>",
	"incorrect": "<p><span>Fail.</span> Sorry. You lose. It actually rains approximately 32 inches a year in Michigan.</p>" // no comma here
},
{ // Question 5
	"q": "Is Earth bigger than a basketball?",
	"a": [
		{"option": "Yes",    "correct": true},
		{"option": "No",     "correct": false} // no comma here
	],
	"correct": "<p><span>Good Job!</span> You must be very observant!</p>",
	"incorrect": "<p><span>ERRRR!</span> What planet Earth are <em>you</em> living on?!?</p>" // no comma here
} // no comma here
						]
				
						}
					
			</script>


			---

			
  ---
  > Blockquotes are very handy to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that is still quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.
		
---
>>>
If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>

---

Inline `code` has `back-ticks around` it.

---

A footnote reference tag looks like this: [^1]

This reference tag is a mix of letters and numbers. [^footnote-42]

[^1]: This is the text inside a footnote.

[^footnote-42]: This is another footnote

---

| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
| cell 10  | <ul><li> - [ ] Task One </li></ul> | <ul><li> - [ ] Task Two </li><li> - [ ] Task Three </li></ul> |

---

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

---

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
---


```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

---


Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you're new to this, don't be :fearful:. You can join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:


---

---

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line

```math
a^2+b^2=c^2
```

---

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2

   ---
   

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

---


